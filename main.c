#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/stat.h>
#include <limits.h>
#include <regex.h>

#define CAPACITY         8 * 1024 * 1024
#define CTLG_LENGTH      2 * 1024 * 1024
#define CTLG_ENTRIES     CTLG_LENGTH / CLUSTER_LENGTH
#define CLUSTER_LENGTH   4096
#define CLUSTER_ENTRIES  CLUSTER_LENGTH / sizeof(handle_t)
#define CLUSTER_EMPTY_B  0x80
#define CLUSTER_AMOUNT_B 0x7F
#define INPUT_LINE_LEN   200
#define FILE_NAME_LEN    8
#define FILE_TYPE_LEN    3
#define TREE_INIT_ERR   -1
#define NO_CHILD_ERR    -2
#define INPUT_ERR       -3
#define LENGTH_ERR      -4
#define CTLG_READ_ERR   -5
#define FILE_NAME_ERR   -6
#define FILE_NAME_EXIST -7
#define NAME_PATTERN "[[:alnum:]_\\-]*"
const char file_pattern[] = "^" NAME_PATTERN "\\.[[:alpha:]]+$";
const char dir_pattern[] = "^" NAME_PATTERN "$";
const char path_pattern[] = "^\\.|(\\./)?((/?" NAME_PATTERN "(/+" NAME_PATTERN ")*|(\\.\\.)(/+\\.\\.))/*)$";

regex_t file_regex, dir_regex, path_regex;

typedef unsigned char byte;

const char *ctlg_name           = "C";
const char *VERSION_INFO        = "DEREGLAZOV NIKITA - Catalogue [version 1.0]";
uint free_space                 = CAPACITY;
uint current_cluster            = 0;
byte current_cluster_entries    = 0;
FILE *ctlg_file;

/*                        FLAG
 *  +-----------------------------------------------+
 *  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
 *  +-----------------------------------------------+
 *  |   UNUSED  |  A  |  D  |  V  |  S  |  H  |  R  |
 *  +-----------------------------------------------+
 */

const byte FILE_TYPE   = 0x00;
const byte READ_ONLY   = 0x01;
const byte HIDDEN      = 0x02;
const byte SYSTEM      = 0x04;
const byte VOLUME_NAME = 0x08;
const byte DIRECTORY   = 0x10;
const byte ARCHIVE     = 0x20;

char *trim(char *str) {
    char *end;
    while (isspace((unsigned char) *str)) str++;
    if (*str == 0) return str;
    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char) *end)) end--;
    *(end + 1) = 0;
    return str;
}

bool filename_valid(char *filename) {
    return filename ? !regexec(&file_regex, filename, 0, NULL, 0) : false;
}

bool dirname_valid(char *dirname) {
    return dirname ? !regexec(&dir_regex, dirname, 0, NULL, 0) : false;
}

bool path_valid(char *path) {
    return path ? !regexec(&path_regex, path, 0, NULL, 0) : false;
}

/**
 * @DESCRIPTION
 * Converts string to uppercase.
*/
void to_uppercase(char *string) {
    for (; *string; ++string) *string = (char) toupper(*string);
}

/**
 * @return Formatted time dd-mm-yyyy hh:mm:ss
*/
char *time_to_string(time_t *time) {
    struct tm info = *localtime(time);
    char *str = (char *) malloc(90 * sizeof(char));

    sprintf(str, "%02d-%02d-%4d %02d:%02d:%02d",
            info.tm_mday,
            info.tm_mon + 1,
            1900 + info.tm_year,
            info.tm_hour,
            info.tm_min,
            info.tm_sec
    );
    return str;
}

/*
 *  +-----------------------------------------------+
 *  |                    TREE                       |
 *  +-----------------------------------------------+
 */

typedef struct {
    char    name[FILE_NAME_LEN];
    char    type[FILE_TYPE_LEN];
    byte    flag;
    uint    cluster;
    time_t  created;
    time_t  access;
    time_t  modified;
    uint    size;
} handle_t;

typedef struct tree_node {
    struct tree_node *right;
    struct tree_node *left;
    handle_t *handle;
    uint handle_address;
} tree_node_t;

typedef struct {
    tree_node_t *root;
    handle_t *handle;
    uint handle_address;
    uint size;
} tree_t;

tree_t *root_tree;
tree_t *current_tree;


bool is_dir(handle_t *handle) {
    return handle->flag & DIRECTORY ? true : false;
}

bool has_attr(handle_t *handle, byte attr) {
    return handle->flag & attr ? true : false;
}

void add_attr(handle_t *handle, byte attr) {
    handle->flag |= attr;
}

void remove_attr(handle_t *handle, byte attr) {
    handle->flag &= !attr;
}

void change_attr(handle_t *handle, byte attr) {
    handle->flag ^= attr;
}

char *attrs_to_string(handle_t *handle) {
    char *buff = malloc(sizeof(char) * 7);
    sprintf(buff, "%s%s%s%s%s%s",
            has_attr(handle, READ_ONLY)   ? "R" : "",
            has_attr(handle, HIDDEN)      ? "H" : "",
            has_attr(handle, SYSTEM)      ? "S" : "",
            has_attr(handle, VOLUME_NAME) ? "V" : "",
            has_attr(handle, DIRECTORY)   ? "D" : "",
            has_attr(handle, ARCHIVE)     ? "A" : ""
    );
    return buff;
}

void init_tree(tree_t *tree, uint handle_address, handle_t *handle) {
    tree->root = calloc(1, sizeof(tree_node_t));
    tree->handle_address = handle_address;
    tree->handle = handle;
}

void concat_name(char *name, char *extension, char *result) {
    strncpy(result, name, FILE_NAME_LEN);
    if (extension[0] == '\0') {
        result[strnlen(result, FILE_NAME_LEN)] = '\0';
        return;
    }
    strcpy(result + strnlen(result, FILE_NAME_LEN), ".");
    strncat(result, extension, FILE_TYPE_LEN);
    result[strnlen(result, FILE_NAME_LEN + FILE_TYPE_LEN + 1)] = '\0';
}

char *handle_to_string(handle_t *handle) {
    char *result = malloc(sizeof(char) * 400);
    char _name[strlen(handle->name) + strlen(handle->type) + 2];
    char *created  = time_to_string(&handle->created);
    char *modified = time_to_string(&handle->modified);
    char *access   = time_to_string(&handle->access);
    char *attrs    = attrs_to_string(handle);
    if (!strlen(attrs)) {
        strcpy(attrs, "-");
    }
    concat_name(handle->name, handle->type, _name);
    sprintf(result,
            "%-12s %s\n%-12s %d Bytes\n%-12s %s\n%-12s %s\n%-12s %s\n%-12s %s\n%-12s 0x%04x\n",
            "File:",       _name,
            "Size:",       handle->size,
            "Created:",    created,
            "Modified:",   modified,
            "Access:",     access,
            "Attributes:", attrs,
            "Address:",    is_dir(handle) ? handle->cluster : CTLG_ENTRIES + 1);
    free(created);
    free(modified);
    free(access);
    free(attrs);
    return result;
}

char *format_handle(handle_t *handle) {
    char *buff = malloc(sizeof(char) * (PATH_MAX + 50));
    char file_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
    char *dir = "<DIR>";
    char size[10];
    concat_name(handle->name, handle->type, file_name);

    sprintf(size, "%u", handle->size);
    sprintf(buff, "%-30s%5s%35s",
            file_name,
            is_dir(handle) ? dir : size,
            time_to_string(&handle->created));

    return buff;
}

/**
 * @return Address of node to add or an existing one.
*/
tree_node_t *search(tree_t *tree, char *name) {
    tree_node_t *temp = tree->root;
    while (temp && temp->handle) {
        char file_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
        concat_name(temp->handle->name, temp->handle->type, file_name);

        int cmp = strcmp(file_name, name);
        if (cmp > 0) {
            if (!temp->left) {
                return temp->left = calloc(1, sizeof(tree_node_t));
            }
            temp = temp->left;
        } else if (cmp < 0) {
            if (!temp->right) {
                return temp->right = calloc(1, sizeof(tree_node_t));
            }
            temp = temp->right;
        } else {
            break;
        }
    }
    return temp;
}

tree_node_t *exist(tree_t *tree, char *name) {
    tree_node_t *temp = tree->root;
    while (temp && temp->handle) {
        char file_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
        concat_name(temp->handle->name, temp->handle->type, file_name);

        int cmp = strcmp(file_name, name);
        if (cmp > 0) {
            temp = temp->left;
        } else if (cmp < 0) {
            temp = temp->right;
        } else {
            return temp;
        }
    }
    return NULL;
}

tree_node_t *__search(tree_t *tree, handle_t *handle) {
    char handle_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
    concat_name(handle->name, handle->type, handle_name);
    tree_node_t *temp = tree->root;

    while (temp && temp->handle) {
        char temp_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
        concat_name(temp->handle->name, temp->handle->type, temp_name);

        int cmp = strcmp(temp_name, handle_name);
        if (cmp > 0) {
            if (!temp->left) {
                return temp->left = calloc(1, sizeof(tree_node_t));
            }
            temp = temp->left;
        } else if (cmp < 0) {
            if (!temp->right) {
                return temp->right = calloc(1, sizeof(tree_node_t));
            }
            temp = temp->right;
        } else {
            break;
        }
    }
    return temp;
}

/**
 * @return  0x10 (OK)    folder
 * @return  0x00 (OK)    file
 * @return  0xFF (ERROR) if filename contains more than one dot
*/
byte __dot_separate(char *file_name, char *extension, char *string) {
    const char *dot = ".";
    char _string[strlen(string) + 1];

    strcpy(_string, string);
    char *rest  = _string;
    char *token = strtok_r(rest, dot, &rest);
    if (strlen(token) + 1 == strlen(string)) {
        strcpy(file_name, "\0");
    } else {
        strncpy(file_name, token, FILE_NAME_LEN);
        file_name[FILE_NAME_LEN] = '\0';
    }

    int tokens_length;
    for (tokens_length = 2; token; tokens_length--) {
        if (tokens_length == 0) {
            return 0xFF;
        }
        strncpy(extension, token, FILE_TYPE_LEN);
        extension[FILE_TYPE_LEN] = '\0';
        token = strtok_r(NULL, dot, &rest);
    }

    if (tokens_length && file_name[0] != '\0') {
        // if folder -> fill extension buffer with "\0"
        strcpy(extension, "\0");
        return DIRECTORY;
    }
    return FILE_TYPE;
}

/**
 * @return  1 (TRUE)  if added
 * @return  0 (FALSE) if name exists
 * @return -1 (ERROR) tree is not initialized
 * @return -6 (ERROR) filename contains more than one dot
 *
 * @DESCRIPTION
 * Set created_node parameter to NULL if it's not necessary to get back created node.
 * Created_node is to be added to catalogue later and it can be NULL if using tree only.
*/
int add_handle(tree_t *tree, char *name, tree_node_t **created_node) {
    if (!name) {
        return false;
    }

    char _file_name[FILE_NAME_LEN];
    char _extension[FILE_TYPE_LEN];
    char _name[strlen(name) + 1];

    byte file_property = __dot_separate(_file_name, _extension, name);
    if (file_property == 0xFF) {
        return FILE_NAME_ERR; // filename contains more than one dot
    }

    concat_name(_file_name, _extension, _name);
    tree_node_t *temp = search(tree, _name);
    handle_t *handle;
    if (temp) {
        if (temp->handle) {
            return false;
        }
        handle = calloc(1, sizeof(handle_t));
        time_t current_time = 0;
        add_attr(handle, file_property);
        strncpy(handle->name, _file_name, FILE_NAME_LEN);
        strncpy(handle->type, _extension, FILE_TYPE_LEN);
        time(&current_time);
        handle->access   = current_time;
        handle->modified = current_time;
        handle->created  = current_time;
        handle->size     = 0;
        handle->cluster  = 0;
        temp->handle     = handle;
        tree->size++;
        if (created_node) *created_node = temp;
    } else {
        return TREE_INIT_ERR;
    }
    return true;
}

/**
 * @return  1 (TRUE)  if added
 * @return  0 (FALSE) if handle exists
 * @return -1 (ERROR) tree is not initialized
*/
int __add_handle(tree_t *tree, handle_t *handle, uint handle_address) {
    tree_node_t *temp = __search(tree, handle);
    if (temp) {
        if (temp->handle) {
            return false;
        }
        temp->handle = handle;
        temp->handle_address = handle_address;
        tree->size++;
    } else {
        return TREE_INIT_ERR;
    }
    return true;
}

/**
 * @DESCRIPTION
 * Rremove node that has not child.
 * If parent == NULL -> there is no parent and tree root is to be removed.
*/
void __remove_node(tree_t *tree, tree_node_t *parent, bool right, bool _free) {
    if (!parent) {
        if (_free) {
            free(tree->root->handle);
            tree->root->handle = NULL;
            free(tree->root);
        }
        tree->root = calloc(1, sizeof(tree_node_t));
    } else {
        if (right) {
            if (_free) {
                free(parent->right->handle);
                parent->right->handle = NULL;
                free(parent->right);
            }
            parent->right = NULL;
        } else {
            if (_free) {
                free(parent->left->handle);
                parent->left->handle = NULL;
                free(parent->left);
            }
            parent->left = NULL;
        }
    }
    tree->size--;
}

/**
 * @return  1 (TRUE)  successfully removed
 * @return -1 (ERROR) if tree is not initialized
 * @return -2 (ERROR) manually chosen parent has no child 'node' try to put NULL to let it find
*/
int remove_node(tree_t *tree, tree_node_t *node, tree_node_t *parent) {
    if (!tree->root) {
        return TREE_INIT_ERR;
    }
    bool right = NULL;
    if (!parent) {
        if (node == tree->root) {
            tree_node_t *temp = NULL;
            if (!node->left && !node->right) {
                __remove_node(tree, parent, right, true);
                return true;
            } else if (!node->left && node->right) {
                temp = node->right;
                __remove_node(tree, parent, right, true);
            } else if (node->left && !node->right) {
                temp = node->left;
                __remove_node(tree, parent, right, true);
            } else {
                temp = node->right;
                tree_node_t *temp_parent = node;
                bool temp_right_to_remove = true;
                while (temp->left) {
                    temp_right_to_remove = false;
                    temp_parent = temp;
                    temp = temp->left;
                }
                __remove_node(tree, temp_parent, temp_right_to_remove, false);
                if (!temp_right_to_remove) {
                    temp_parent->left = temp->right;
                    temp->right = node->right;
                }
                temp->left = node->left;
                __remove_node(tree, parent, right, true);
                tree->size++;
            }
            tree->root = temp;
        } else {
            // Find parent and call remove_node again
            tree_node_t *temp = tree->root;
            tree_node_t *_parent = NULL;

            char name2[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
            concat_name(node->handle->name, node->handle->type, name2);

            while (temp && temp->handle) {
                char name1[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
                strcpy(name1, temp->handle->name);
                if (strlen(temp->handle->type)) {
                    strcat(name1, ".");
                    strcat(name1, temp->handle->type);
                }
                int cmp = strcmp(name1, name2);
                if (cmp > 0) {
                    _parent = temp;
                    temp = temp->left;
                } else if (cmp < 0) {
                    _parent = temp;
                    temp = temp->right;
                } else {
                    remove_node(tree, temp, _parent);
                }
            }
            return NO_CHILD_ERR;
        }
    } else {
        if (parent->right == node) {
            right = true;
        } else if (parent->left == node) {
            right = false;
        } else {
            return NO_CHILD_ERR;
        }
        tree_node_t *temp = NULL;
        // Node to be removed has not children >>>
        if (!node->left && !node->right) {
            __remove_node(tree, parent, right, true);
        // Node to be removed has one child >>>
        } else if (!node->left && node->right) {
            temp = node->right;
            __remove_node(tree, parent, right, true);
        } else if (node->left && !node->right) {
            temp = node->left;
            __remove_node(tree, parent, right, true);
        // Node to be removed has two children >>>
        } else {
            temp = node->right;
            tree_node_t *temp_parent = node;
            bool temp_right_to_remove = true;
            while (temp->left) {
                temp_right_to_remove = false;
                temp_parent = temp;
                temp = temp->left;
            }
            __remove_node(tree, temp_parent, temp_right_to_remove, false);
            if (!temp_right_to_remove) {
                temp_parent->left = temp->right;
                temp->right = node->right;
            }
            temp->left = node->left;
            __remove_node(tree, parent, right, true);
            tree->size++;
        }
        if (right) {
            parent->right = temp;
        } else {
            parent->left = temp;
        }
    }
    return true;
}

/**
 * @return  1 (TRUE)  if deleted
 * @return  0 (FALSE) if name does not exist
 * @return -1 (ERROR) if tree is not initialized
 * @return -2 (ERROR) parent has no child 'node'
 *
 * @DESCRIPTION
 * set cluster and handle_address parameter to NULL if it's not necessary to get back renamed handle
 * cluster and handle_address are to be deleted from catalogue later and it can be NULL if using tree only
*/
int delete_handle(tree_t *tree, char *name, uint *cluster, uint *handle_address) {
    if (!name) {
        return false;
    }

    tree_node_t *temp = tree->root;
    tree_node_t *parent = NULL;
    if (!temp) {
        return TREE_INIT_ERR;
    }
    while (temp && temp->handle) {
        char file_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
        concat_name(temp->handle->name, temp->handle->type, file_name);
        int cmp = strcmp(file_name, name);
        if (cmp > 0) {
            parent = temp;
            temp = temp->left;
        } else if (cmp < 0) {
            parent = temp;
            temp = temp->right;
        } else {
            if (cluster != NULL) *cluster = temp->handle->cluster;
            if (handle_address != NULL) *handle_address = temp->handle_address;
            return remove_node(tree, temp, parent);
        }
    }
    return false;
}

/**
 * @return  1 (TRUE)  if deleted
 * @return  0 (FALSE) if n1 does not exist
 * @return -1 (ERROR) if tree is not initialized
 * @return -2 (ERROR) parent has no child 'node'
 * @return -6 (ERROR) filename (n2) contains more than one dot
 * @return -7 (ERROR) if n2 exist
 *
 * @DESCRIPTION
 * Find n1 (true) -> find n2 (false) -> remember n1 -> delete n1 -> add n2 (with attributes of n1).
 * Set renamed_node parameter to NULL if it's not necessary to get back node with renamed handle.
 * Renamed_node is to be deleted from catalogue later and it can be NULL if using tree only.
*/
int rename_handle(tree_t *tree, char *n1, char *n2, tree_node_t **renamed_node) {
    tree_node_t *temp_n1 = tree->root;
    tree_node_t *parent = NULL;
    if (!temp_n1) {
        return TREE_INIT_ERR;
    }
    while (temp_n1 && temp_n1->handle) {
        char file_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
        concat_name(temp_n1->handle->name, temp_n1->handle->type, file_name);

        int cmp = strcmp(file_name, n1);
        if (cmp > 0) {
            parent = temp_n1;
            temp_n1 = temp_n1->left;
        } else if (cmp < 0) {
            parent = temp_n1;
            temp_n1 = temp_n1->right;
        } else { // n1 exist
            if (!exist(tree, n2)) { // if n2 is not in tree
                char _file_name[FILE_NAME_LEN];
                char _extension[FILE_TYPE_LEN];
                char _n2[strlen(n2) + 1];

                byte file_property = __dot_separate(_file_name, _extension, n2);
                if (file_property == 0xFF) return FILE_NAME_ERR; // filename contains more than one dot
                concat_name(_file_name, _extension, _n2);

                handle_t *handle = calloc(1, sizeof(handle_t));
                time_t current_time = 0;
                time(&current_time);
                handle->modified    = current_time;
                handle->access      = temp_n1->handle->access;
                handle->created     = temp_n1->handle->created;
                handle->size        = temp_n1->handle->size;
                handle->cluster     = temp_n1->handle->cluster;
                handle->flag        = temp_n1->handle->flag;
                uint handle_address = temp_n1->handle_address;

                int remove_result = remove_node(tree, temp_n1, parent);
                if (!remove_result) {
                    free(handle);
                    return remove_result; // ERROR
                }
//                add_attr(handle, file_property);
                strcpy(handle->name, _file_name);
                strcpy(handle->type, _extension);
                tree_node_t *new_node = search(tree, n2);
                new_node->handle_address = handle_address;
                new_node->handle = handle;
                if (renamed_node) *renamed_node = new_node;
                tree->size++;
                return true;
            }
            return FILE_NAME_EXIST;
        }
    }
    return false;
}

void __collect_tree_nodes(tree_node_t *node, handle_t **array, uint size) {
    static uint index = 0;
    if (index == size) {
        index = 0;
    }
    if (!node) {
        return;
    }
    char name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
    concat_name(node->handle->name, node->handle->type, name);
    array[index++] = node->handle;
    __collect_tree_nodes(node->left, array, size);
    __collect_tree_nodes(node->right, array, size);
}

handle_t** tree_to_array(tree_t *tree) {
    if (tree->size > 0) {
        handle_t** array = malloc(sizeof(handle_t*) * tree->size);
        __collect_tree_nodes(tree->root, array, tree->size);
        return array;
    }
    return NULL;
}

/**
 * @DESCRIPTION
 * Insertion sort of array
*/
void sort(handle_t **array, uint size) {
    char n1[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
    char n2[FILE_NAME_LEN + FILE_TYPE_LEN + 2];

    for (int j = 1; j < size; ++j) {
        handle_t *temp = array[j];
        int i = j;
        concat_name(array[i - 1]->name, array[i - 1]->type, n1);
        concat_name(temp->name, temp->type, n2);

        while (strcmp(n1, n2) > 0) {
            array[i] = array[i - 1];
            i--;
            if (i <= 0) {
                break;
            }
            concat_name(array[i - 1]->name, array[i - 1]->type, n1);
        }
        array[i] = temp;
    }
}

void __free_nodes(tree_node_t *root) {
    if (root) {
        __free_nodes(root->left);
        __free_nodes(root->right);
        free(root->handle);
        free(root);
    }
}

void clear_tree(tree_t *tree) {
    __free_nodes(tree->root);
    tree->root = calloc(1, sizeof(tree_node_t));
    tree->size = 0;
}

void destroy_tree(tree_t *tree) {
    __free_nodes(tree->root);
    free(tree);
}


/*
 *  +-----------------------------------------------+
 *  |                   STACK                       |
 *  +-----------------------------------------------+
 */

typedef struct stack_node {
    struct stack_node *next;
    tree_t *tree;
} stack_node_t;

typedef struct {
    stack_node_t *top;
    uint size;
} stack_t;

stack_t *dir_stack;

void init_stack(stack_t **stack) {
    *stack = calloc(1, sizeof(stack_t));
}

void push(stack_t *stack, tree_t *tree) {
    stack_node_t *temp = malloc(sizeof(stack_node_t));
    temp->next = stack->top;
    temp->tree = tree;
    stack->top = temp;
    stack->size++;
}

/**
 * @return Tree of the popped node
*/
tree_t *pop(stack_t *stack) {
    if (!stack->size) {
        return NULL;
    }
    stack_node_t *node = stack->top;
    stack->top = stack->top->next;
    tree_t *tree = node->tree;
    free(node);
    stack->size--;
    return tree;
}

void __stack_to_string(stack_t *stack, stack_node_t *node, char *string) {
    if (!node) {
        return;
    }
    __stack_to_string(stack, node->next, string);
    char _name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
    concat_name(node->tree->handle->name, node->tree->handle->type, _name);
    strcat(string, _name);
    if (node != stack->top) {
        strcat(string, "\\");
    }
}

/**
 * DESCRIPTION:
 * Builds path using stack. If there is not root directory put NULL instead of root_name.
*/
char *stack_to_string(stack_t *stack, char *root_name) {
    char *result_str = malloc(sizeof(char) * PATH_MAX);
    if (root_name) {
        strcpy(result_str, root_name);
        strcat(result_str, ":\\");
    }
    __stack_to_string(stack, stack->top, result_str);
    strcat(result_str, ">");
    return result_str;
}

void clear_stack(stack_t *stack) {
    while (stack->size > 0) {
        tree_t *temp = pop(stack);
        destroy_tree(temp);
    }
}

void destroy_stack(stack_t *stack) {
    clear_stack(stack);
    free(stack);
}

tree_t *get_top(stack_t *stack) {
    return stack->top->tree;
}

bool is_empty(stack_t *stack) {
    return stack->size > 0 ? false : true;
}

/*
 *  +-----------------------------------------------+
 *  |                    CTLG                       |
 *  +-----------------------------------------------+
 */

/**
 * @return number of free cluster
 * @return -1 if there is no free cluster in ctlg
*/
ssize_t get_free_cluster() {
    uint cluster = 0;
    fseek(ctlg_file, CLUSTER_LENGTH, SEEK_SET);
    byte cluster_label;
    while (true) {
        cluster++;
        fread(&cluster_label, sizeof(byte), 1, ctlg_file);
        if ((cluster_label & CLUSTER_EMPTY_B) == 0) {
            return cluster;
        }
        if (cluster == CTLG_ENTRIES) return -1;
        fseek(ctlg_file, CLUSTER_LENGTH - 1, SEEK_CUR);
    }
}

/**
 * @return amount of entries of current cluster
*/
byte get_current_cluster_entries() {
    fseek(ctlg_file, current_cluster * CLUSTER_LENGTH, SEEK_SET);
    byte cluster_label;
    fread(&cluster_label, sizeof(byte), 1, ctlg_file);
    return (byte) (cluster_label & CLUSTER_EMPTY_B ? cluster_label & CLUSTER_AMOUNT_B : 0);
}

off_t file_size(FILE *file) {
    struct stat st;
    return !fstat(fileno(file), &st) ? st.st_size : -1;
}

/**
 * @returns amount of loaded clusters
*/
uint load_dir(tree_t *tree, uint start_cluster, uint *last_cluster, byte *cluster_entries) {
    uint loaded = 0;
    byte cluster_label;
    uint cluster = start_cluster;
    byte entries = 0;
    fseek(ctlg_file, cluster * CLUSTER_LENGTH, SEEK_SET);
    fread(&cluster_label, sizeof(byte), 1, ctlg_file);
    bool cluster_not_empty = cluster_label & CLUSTER_EMPTY_B ? true : false;
    if (cluster_not_empty) {
        entries = (byte) (cluster_label & CLUSTER_AMOUNT_B);
        do {
            handle_t *handle;
            for (int i = 0; i < entries; ++i) {
                handle = calloc(1, sizeof(handle_t));
                fread(handle, sizeof(handle_t), 1, ctlg_file);
                __add_handle(tree, handle, cluster * CLUSTER_LENGTH + sizeof(handle_t) * i + sizeof(byte));
                loaded++;
            }
            if (entries == CLUSTER_ENTRIES) {
                uint next_cluster_ptr;
                fseek(ctlg_file, cluster * CLUSTER_LENGTH + CLUSTER_LENGTH - sizeof(uint), SEEK_SET);
                fread(&next_cluster_ptr, sizeof(uint), 1, ctlg_file);
                if (next_cluster_ptr) {
                    cluster = next_cluster_ptr;
                    fseek(ctlg_file, cluster * CLUSTER_LENGTH, SEEK_SET);
                    fread(&cluster_label, sizeof(byte), 1, ctlg_file);
                    cluster_not_empty = cluster_label & CLUSTER_EMPTY_B ? true : false;
                    if (!cluster_not_empty) break;
                    entries = (byte) (cluster_label & CLUSTER_AMOUNT_B);
                } else break;
            } else break;
        } while (entries);
    }
    *last_cluster = cluster;
    *cluster_entries = entries;
    return loaded;
}

void create_ctlg_workspace(off_t size) {
    byte zero  = 0;
    for (int i = 0; i < size; ++i) {
        fwrite(&zero, sizeof(byte), 1, ctlg_file);
    }
    fflush(ctlg_file);
};

void extend_ctlg_workspace() {
    off_t f_size = file_size(ctlg_file);
    off_t size_to_add;
    if (f_size < CTLG_LENGTH) {
        size_to_add = CTLG_LENGTH - f_size;
        create_ctlg_workspace(size_to_add);
    }
}

void format_ctlg(bool flag) {
    rewind(ctlg_file);
    if (flag) {
        byte zero = 0;
        for (int i = 0; i < CTLG_ENTRIES; ++i) {
            fwrite(&zero, sizeof(zero), 1, ctlg_file);
            fseek(ctlg_file, CLUSTER_LENGTH - 9, SEEK_CUR);
            fwrite(&zero, sizeof(zero), 8, ctlg_file);
        }
    } else {
        create_ctlg_workspace(CTLG_LENGTH);
    }
}

void create_ctlg() {
    FILE *ctlg = fopen(ctlg_name, "wb");
    fclose(ctlg);
    ctlg_file = fopen(ctlg_name, "rb+");
    if (!ctlg_file) {
        printf("Can't create catalogue \"%s\".\n\n", ctlg_name);
        exit(CTLG_READ_ERR);
    }
    create_ctlg_workspace(CTLG_LENGTH);
}

/**
 * @return  1 (TRUE)  if catalogue successfully loaded
 * @return  0 (FALSE) otherwise
*/
int load_ctlg() {
    ctlg_file = fopen(ctlg_name, "rb+");
    if (!ctlg_file) {
        create_ctlg();
    }
    off_t ctlg_size = file_size(ctlg_file);
    if (ctlg_size == CTLG_LENGTH) {
        uint entries_loaded = load_dir(root_tree, 0, &current_cluster, &current_cluster_entries);
        printf("%u entries of %s loaded.\n\n", entries_loaded, ctlg_name);
    } else if (ctlg_size > 0) {
        printf("Illegal catalogue size: %li Bytes.\n\n", ctlg_size);
        exit(CTLG_READ_ERR);
    } else {
        exit(CTLG_READ_ERR);
    }
    return true;
}

/*
 *  +-----------------------------------------------+
 *  |                    MENU                       |
 *  +-----------------------------------------------+
 */

/**
 * @return   1 (TRUE)  if input has no errors
 * @return  -3 (ERROR) if input has illegal symbols
 * @return  -4 (ERROR) if length of input is out of buffer
*/
int line_input(char *message, char *buff, uint sz) {
    int temp_char;
    int len_flag;
    size_t last;

    if (message) {
        printf("%s", message);
        fflush(stdout);
    }
    if (!fgets(buff, (int) sz, stdin)) {
        return INPUT_ERR;
    }
    last = strlen(buff) - 1;
    if (buff[last] != '\n') {
        len_flag = 0;
        while (((temp_char = getchar()) != '\n') && (temp_char != EOF)) {
            len_flag = 1;
        }
        return len_flag == 1 ? LENGTH_ERR : true;
    }
    buff[last] = '\0';
    return true;
}

tree_t *__cd(tree_t *dir_tree, char *dir, uint *last_cluster, byte *cluster_entries) {
    if (dirname_valid(dir)) {
        tree_node_t *_dir = exist(dir_tree, dir);
        if (_dir) {
            if (is_dir(_dir->handle)) {
                tree_t *temp_tree = calloc(1, sizeof(tree_t));
                uint start_cluster = _dir->handle->cluster;
                init_tree(temp_tree, _dir->handle_address, _dir->handle);
                load_dir(temp_tree, start_cluster, last_cluster, cluster_entries);
                return temp_tree;
            } else {
                puts("Chosen file is not directory.\n");
                return NULL;
            }
        }
    }
    return NULL;
}

void cd(char *args, char *current_dir) {
    if (args && strlen(args = trim(args)) && (strcmp(args, "/"))) {
        if (path_valid(args)) {
            size_t args_len = strlen(args);
            if (args_len == 1 && args[0] == '.') return;
            if (args_len > 1 && (!strncmp(current_dir, args, strlen(current_dir) - 1))) return;
            uint path_len = (uint) (strlen(args) + 1);
            if (path_len > PATH_MAX) {
                printf("Path error. Max path length: %u.\n\n", PATH_MAX);
                return;
            }
            char path[path_len];
            strncpy(path, args, path_len);

            char *path_pointer = path;
            char *rest = path_pointer;
            bool from_root = false;
            if (!strncmp(path, "./", 2)) {
                path_pointer += 2;
            } else if (path[0] == '/') {
                from_root = true;
                path_pointer++;
            }

            char *token = strtok_r(path_pointer, "/", &rest);
            if (token) {
                while (!strcmp(token, "..")) {
                    if (!is_empty(dir_stack)) {
                        tree_t *temp_tree = pop(dir_stack);
                        destroy_tree(temp_tree);
                        current_tree = is_empty(dir_stack) ? root_tree : get_top(dir_stack);
                        current_cluster = current_tree->handle ? current_tree->handle->cluster : 0;
                        current_cluster_entries = (byte) get_current_cluster_entries();
                    }
                    if (!(token = strtok_r(NULL, "/", &rest))) goto update_last_access;
                }
            }
            bool first = true;
            uint jumps = 0;
            uint _current_cluster = current_cluster;
            byte _cluster_entries = current_cluster_entries;

            stack_t *temp_stack = from_root ? malloc(sizeof(stack_t)) : dir_stack;
            do {
                tree_t *next_tree;
                if (first) {
                    next_tree = __cd(from_root ? root_tree : current_tree, token, &_current_cluster, &_cluster_entries);
                    first = false;
                } else {
                    next_tree = __cd(get_top(temp_stack), token, &_current_cluster, &_cluster_entries);
                }
                if (next_tree) {
                    push(temp_stack, next_tree);
                    jumps++;
                } else {
                    puts("File not found.\n");
                    goto rollback;
                }
            } while ((token = strtok_r(NULL, "/", &rest)));

            if (from_root) {
                destroy_stack(dir_stack);
                dir_stack = temp_stack;
            }

            current_cluster = _current_cluster;
            current_cluster_entries = _cluster_entries;
            current_tree = get_top(temp_stack);

            update_last_access:
            if (current_tree->handle) {
                time_t current_time;
                time(&current_time);
                current_tree->handle->access = current_time;

                fseek(ctlg_file, current_tree->handle_address, SEEK_SET);
                fwrite(current_tree->handle, sizeof(handle_t), 1, ctlg_file);
                fflush(ctlg_file);
            }
            char *new_dir = stack_to_string(dir_stack, (char *) ctlg_name);
            strcpy(current_dir, new_dir);
            free(new_dir);
            return;
            rollback:
            for (uint i = 0; i < jumps; ++i) {
                tree_t *temp_tree = pop(temp_stack);
                destroy_tree(temp_tree);
            }
        } else {
            puts("Illegal path.\n");
        }
    } else if (current_tree != root_tree) {
        current_tree = root_tree;
        rewind(ctlg_file);
        byte cluster_label;
        fread(&cluster_label, sizeof(byte), 1, ctlg_file);

        uint next_cluster_ptr = 0;
        bool cluster_not_empty = cluster_label & CLUSTER_EMPTY_B ? true : false;
        byte cluster_entries = 0;
        if (cluster_not_empty) {
            cluster_entries = (byte) (cluster_label & CLUSTER_AMOUNT_B);
            do {
                if (cluster_entries == CLUSTER_ENTRIES) {
                    fseek(ctlg_file, next_cluster_ptr * CLUSTER_LENGTH + CLUSTER_LENGTH - sizeof(uint), SEEK_SET);
                    fread(&next_cluster_ptr, sizeof(uint), 1, ctlg_file);
                    if (next_cluster_ptr) {
                        fseek(ctlg_file, next_cluster_ptr * CLUSTER_LENGTH, SEEK_SET);
                        fread(&cluster_label, sizeof(byte), 1, ctlg_file);
                        cluster_not_empty = cluster_label & CLUSTER_EMPTY_B ? true : false;
                        if (!cluster_not_empty) break;
                        cluster_entries = (byte) (cluster_label & CLUSTER_AMOUNT_B);
                    } else break;
                } else break;
            } while (cluster_entries);
        }

        current_cluster = next_cluster_ptr;
        current_cluster_entries = cluster_entries;
        clear_stack(dir_stack);
        char *new_dir = stack_to_string(dir_stack, (char *) ctlg_name);
        strcpy(current_dir, new_dir);
    }
}

/**
  * @return formatted string of command description
*/
char *command_description(char *cmd) {
    char *result = malloc(sizeof(char) * (strlen(cmd) + 45));
    char _cmd[strlen(cmd) + 1];
    char *description;
    strcpy(_cmd, cmd);
    to_uppercase(_cmd);

    if (!strcmp(_cmd, "DIR")) {
        description = "Directory View.";
    } else if (!strcmp(_cmd, "CD")) {
        description = "Displays/changes the current directory.";
    } else if (!strcmp(_cmd, "CLS")) {
        description = "Clear screen.";
    } else if (!strcmp(_cmd, "MKDIR") || !strcmp(_cmd, "MD")) {
        description = "Make Directory or file.";
    } else if (!strcmp(_cmd, "DEL")) {
        description = "Removes file.";
    } else if (!strcmp(_cmd, "REN")) {
        description = "Renames one or more files.";
    } else if (!strcmp(_cmd, "FMT")) {
        description = "Format 'catalogue', [-f] fast format.";
    } else if (!strcmp(_cmd, "INFO")) {
        description = "Displays file informaion";
    } else if (!strcmp(_cmd, "HELP")) {
        description = "Show commands and their description.";
    } else if (!strcmp(_cmd, "EXIT")) {
        description = "Exit from the 'Catalogue'.";
    } else if (!strcmp(_cmd, "VER")) {
        description = "Show 'Catalogue' version.";
    } else {
        description = "Unknown command.";
    }
    sprintf(result, "<%-5s> %s", _cmd, description);
    return result;
}

/**
 * @DESCRIPTION
 * Prints 'arg' command description.
 * Prints all existing commands if args is NULL or empty.
*/
void help(char *arg) {
    if (arg && strlen(arg) > 0) {
        puts(command_description(arg));
    } else {
        puts(command_description("dir"));
        puts(command_description("cd"));
        puts(command_description("cls"));
        puts(command_description("del"));
        puts(command_description("exit"));
        puts(command_description("md"));
        puts(command_description("mkdir"));
        puts(command_description("info"));
        puts(command_description("fmt"));
        puts(command_description("help"));
        puts(command_description("ren"));
        puts(command_description("ver"));
    }
    puts("");
}

/**
  * @return TRUE and print handle info if file exists
  * @return FALSE otherwise
*/
bool __info(tree_t *tree, char *name) {
    tree_node_t *temp = tree->root;
    while (temp && temp->handle) {
        char file_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
        concat_name(temp->handle->name, temp->handle->type, file_name);

        int cmp = strcmp(file_name, name);
        if (cmp > 0) {
            if (!temp->left) {
                break;
            }
            temp = temp->left;
        } else if (cmp < 0) {
            if (!temp->right) {
                break;
            }
            temp = temp->right;
        } else {
            puts(handle_to_string(temp->handle));
            return true;
        }
    }
    return false;
}

/**
 * @DESCRIPTION
 * Split by space args (file names) and displays information of each file.
 * If no file exists returns error message.
*/
void info(char *args) {
    char *syntax_error = "Command syntax error.\n";
    if (!args) {
        puts(syntax_error);
        return;
    }
    char temp[strlen(args) + 1];
    strcpy(temp, args);

    char *rest  = temp;
    char *token = strtok_r(rest, " ", &rest);
    if (!token) {
        puts(syntax_error);
        return;
    }
    int info_result;
    bool any_found = true;
    do {
        info_result = __info(current_tree, token);
        if (!any_found || info_result == true) {
            any_found = false;
        }
    } while ((token = strtok_r(NULL, " ", &rest)));
    if (any_found) {
        puts("Can't find file(s) to display info.\n");
    }
}

void fmt(char *args) {
    if (!args) {
        format_ctlg(false);
        return;
    }
    char temp[strlen(args) + 1];
    strcpy(temp, args);

    char *rest  = temp;
    char *token = strtok_r(rest, " ", &rest);
    if (!token) {
        format_ctlg(false);
        clear_stack(dir_stack);
        clear_tree(root_tree);
        current_cluster = 0;
        current_cluster_entries = 0;
    } else if (!strcasecmp(token, "-f") && !(strtok_r(NULL, " ", &rest))) {
        format_ctlg(true);
        clear_stack(dir_stack);
        clear_tree(root_tree);
        current_cluster = 0;
        current_cluster_entries = 0;
    } else {
        puts("Command syntax error.\n");
    }
}

void dir() {
    handle_t **array = tree_to_array(current_tree);
    sort(array, current_tree->size);
    uint files_size    = 0;
    uint dirs_counter  = 0;
    uint files_counter = 0;

    for (int i = 0; i < current_tree->size; ++i) {
        if (is_dir(array[i])) {
            dirs_counter++;
        } else {
            files_counter++;
            files_size += array[i]->size;
        }
        printf("%s\n", format_handle(array[i]));
    }
    printf("%35d File(s) %8d Bytes.\n", files_counter, files_size);
    printf("%35d Dir(s)  %8d Bytes free.\n\n", dirs_counter, free_space);
}

/**
 * @DESCRIPTION
 * Split by space args (file names) and create files one by one.
 * Creates only that files which do not exist.
*/
void md(char *args) {
    char *syntax_error = "Command syntax error.\n";
    if (!args) {
        puts(syntax_error);
        return;
    }
    char temp[strlen(args) + 1];
    strcpy(temp, args);

    char *rest  = temp;
    char *token = strtok_r(rest, " ", &rest);
    if (!token) {
        puts(syntax_error);
        return;
    }
    int create_result;
    do {
        bool dir = strchr(token, '.') ? false : true;
        if (dir) {
            if (!dirname_valid(token)) {
                printf("Illegal symbols in directory name \"%s\".\n\n", token);
                continue;
            }
        } else {
            if (!filename_valid(token)) {
                printf("Illegal symbols in filename \"%s\".\n\n", token);
                continue;
            }
        }
        tree_node_t *created_node = NULL;
        create_result = add_handle(current_tree, token, &created_node);
        if (create_result == true) {
            bool jump = false;
            if (current_cluster_entries == CLUSTER_ENTRIES) {
                ssize_t free_cluster = get_free_cluster();
                if (free_cluster == -1) {
                    puts("Catalogue overflow.\n");
                    return;
                }

                uint free = (uint) free_cluster;
                fseek(ctlg_file, current_cluster * CLUSTER_LENGTH + CLUSTER_LENGTH - 2 * sizeof(uint), SEEK_SET);
                if (current_cluster != (current_tree->handle ? current_tree->handle->cluster : 0)) {
                    fwrite(&free, sizeof(uint), 1, ctlg_file);
                } else {
                    uint none = (uint) -1;
                    fwrite(&none, sizeof(uint), 1, ctlg_file);
                }
                current_cluster = (uint) free_cluster;
                current_cluster_entries = 0;
                jump = true;
            }
            created_node->handle_address = current_cluster * CLUSTER_LENGTH
                                           + current_cluster_entries * sizeof(handle_t) + sizeof(byte);

            ssize_t cluster = CLUSTER_ENTRIES + 1;
            if (dir) {
                cluster = get_free_cluster();
                if (cluster == -1) {
                    puts("Catalogue overflow.\n");
                    return;
                }
            }
            created_node->handle->cluster = (uint) cluster;
            if (jump) {
                fwrite(&current_cluster, sizeof(uint), 1, ctlg_file);
            }
            fseek(ctlg_file, current_cluster * CLUSTER_LENGTH, SEEK_SET);
            current_cluster_entries++;

            byte cluster_label = (byte) (CLUSTER_EMPTY_B | current_cluster_entries);
            fwrite(&cluster_label, sizeof(byte), 1, ctlg_file);
            fseek(ctlg_file, (current_cluster_entries - 1) * sizeof(handle_t), SEEK_CUR);
            fwrite(created_node->handle, sizeof(handle_t), 1, ctlg_file);

            if (dir) {
                fseek(ctlg_file, cluster * CLUSTER_LENGTH, SEEK_SET);
                cluster_label = CLUSTER_EMPTY_B;
                fwrite(&cluster_label, sizeof(byte), 1, ctlg_file);
            }
            fflush(ctlg_file);
        } else if (!create_result) {
            printf("File with name \"%s\" already exists.\n\n", token);
        } else if (create_result == FILE_NAME_ERR) {
            printf("Filename \"%s\" contains more than one dot.\n\n", token);
        }
    } while ((token = strtok_r(NULL, " ", &rest)));
}

/**
 * @DESCRIPTION
 * Split by space args (file names) and delete files one by one.
 * Deletes only existing files, if no file exists prints error message.
*/
void del(char *args) {
    char *syntax_error = "Command syntax error.\n";
    if (!args) {
        puts(syntax_error);
        return;
    }
    char temp[strlen(args) + 1];
    strcpy(temp, args);

    char *rest  = temp;
    char *token = strtok_r(rest, " ", &rest);
    if (!token) {
        puts(syntax_error);
        return;
    }
    int delete_result;
    bool any_deleted = true;
    do {
        uint cluster;
        uint handle_address;
        delete_result = delete_handle(current_tree, token, &cluster, &handle_address);
        if (delete_result == true) {
            any_deleted = false;
            const byte zero = 0;
            if (dir) {
                fseek(ctlg_file, cluster * CLUSTER_LENGTH, SEEK_SET);
                uint next_cluster_ptr = 0;
                do {
                    fwrite(&zero, sizeof(byte), 1, ctlg_file);
                    fseek(ctlg_file, CLUSTER_LENGTH - 1 - 2 * sizeof(uint), SEEK_CUR);
                    fwrite(&zero, sizeof(uint), 1, ctlg_file);
                    fread(&next_cluster_ptr, sizeof(uint), 1, ctlg_file);
                    fseek(ctlg_file, -sizeof(uint), SEEK_CUR);
                    fwrite(&zero, sizeof(uint), 1, ctlg_file);
                } while (next_cluster_ptr);
            }

            handle_t handle;
            byte cluster_label;
            fseek(ctlg_file, current_cluster * CLUSTER_LENGTH, SEEK_SET);
            fread(&cluster_label, sizeof(byte), 1, ctlg_file);

            byte cluster_entries = (byte) (cluster_label & CLUSTER_AMOUNT_B);
            if ((cluster_label & CLUSTER_EMPTY_B) && cluster_entries) {
                if (current_cluster == (current_tree->handle ? current_tree->handle->cluster : 0)) {
                    if (cluster_entries == 1) {
                        fseek(ctlg_file, -sizeof(byte), SEEK_CUR);
                        cluster_label = CLUSTER_EMPTY_B;
                        current_cluster_entries = 0;
                        fwrite(&cluster_label, sizeof(byte), 1, ctlg_file);
                        fflush(ctlg_file);
                        continue;
                    }
                    uint deleted_index = (handle_address - current_cluster * CLUSTER_LENGTH) / sizeof(handle_t) + 1;
                    if (deleted_index == cluster_entries) {
                        fseek(ctlg_file, -sizeof(byte), SEEK_CUR);
                        current_cluster_entries--;
                        cluster_label = (byte) (CLUSTER_EMPTY_B | current_cluster_entries);
                        fwrite(&cluster_label, sizeof(byte), 1, ctlg_file);
                        fflush(ctlg_file);
                        continue;
                    }
                }

                fseek(ctlg_file, (cluster_entries - 1) * sizeof(handle_t), SEEK_CUR);
                fread(&handle, sizeof(handle_t), 1, ctlg_file);
                fseek(ctlg_file, current_cluster * CLUSTER_LENGTH, SEEK_SET);
                if (cluster_entries > 1) {
                    cluster_label--;
                    fwrite(&cluster_label, sizeof(byte), 1, ctlg_file);
                    current_cluster_entries--;
                } else {
                    if (current_cluster == cluster) {
                        cluster_label = CLUSTER_EMPTY_B;
                        fwrite(&cluster_label, sizeof(byte), 1, ctlg_file);
                        current_cluster_entries = 0;
                    } else {
                        cluster_label = 0;
                        fwrite(&cluster_label, sizeof(byte), 1, ctlg_file);
                        fseek(ctlg_file, CLUSTER_LENGTH - 2 * sizeof(handle_t) - 1, SEEK_CUR);
                        fread(&current_cluster, sizeof(uint), 1, ctlg_file);
                        current_cluster_entries = get_current_cluster_entries();
                        fseek(ctlg_file, -sizeof(uint), SEEK_CUR);
                        fwrite(&zero, sizeof(uint), 1, ctlg_file);
                    }

                }
                handle_t *temp_handle = &handle;
                if (temp_handle) {
                    char handle_name[FILE_NAME_LEN + FILE_TYPE_LEN + 2];
                    concat_name(temp_handle->name, temp_handle->type, handle_name);
                    tree_node_t *temp_node = exist(current_tree, handle_name);
                    if (temp_node) {
                        temp_node->handle_address = handle_address;
                    } else {
                        exit(NO_CHILD_ERR);
                    }
                    fseek(ctlg_file, handle_address, SEEK_SET);
                    fwrite(&handle, sizeof(handle_t), 1, ctlg_file);
                    fflush(ctlg_file);
                } else {
                    printf("Error while deleting %s \"%s\".\n\n", dir ? "directory" : "file", token);
                }
            } else {
                printf("Error while deleting %s \"%s\".\n\n", dir ? "directory" : "file", token);
                continue;
            }
        }
    } while ((token = strtok_r(NULL, " ", &rest)));
    if (any_deleted) {
        puts("Can't find file(s) to delete.\n");
    }
}

/**
 * @DESCRIPTION
 * Split by space args (file names) and rename file arg1 to arg2.
 * Renames if arg1 exists and arg2 do not, otherwise returns error message.
 * Prints error message if there is more than two args.
*/
void ren(char *args) {
    char *syntax_error = "Command syntax error.\n";
    if (!args) {
        puts(syntax_error);
        return;
    }
    char temp[strlen(args) + 1];
    strcpy(temp, args);

    char *n1 = strtok(temp, " ");
    if (!n1) {
        puts(syntax_error);
        return;
    }
    char *n2 = strtok(NULL, " ");
    if (!n2 || strtok(NULL, " ")) {
        puts(syntax_error);
        return;
    }

    bool dir = strchr(n1, '.') ? false : true;
    if (dir ? !(dirname_valid(n1) && dirname_valid(n2)) : !(filename_valid(n1) && filename_valid(n2))) {
        printf("Illegal symbols in %s name.\n\n", dir ? "directory" : "file");
        return;
    }
    tree_node_t *renamed_node = NULL;
    int rename_result = rename_handle(current_tree, n1, n2, &renamed_node);
    if (rename_result == true) {
        fseek(ctlg_file, renamed_node->handle_address, SEEK_SET);
        fwrite(renamed_node->handle, sizeof(handle_t), 1, ctlg_file);
        fflush(ctlg_file);
    } else if (!rename_result) {
        printf("Can't find %s \"%s\".\n\n", dir ? "directory" : "file", n1);
    } else if (rename_result == FILE_NAME_EXIST) {
        printf("%s \"%s\" already exists.\n\n", dir ? "Directory" : "File", n2);
    } else {
        printf("Unexpected error: %d.\n\n", rename_result);
    }
}

int __exit() {
    int code = 0;
    destroy_tree(root_tree);
    destroy_stack(dir_stack);
    fclose(ctlg_file);
    return code;
}

void menu(stack_t *dir_stack) {
    char *current_dir = stack_to_string(dir_stack, (char *) ctlg_name);
    char input_buff[INPUT_LINE_LEN];
    char *input_right;
    char *cmd;
    int input_result;

    continue_point:
    input_result = line_input(current_dir, input_buff, sizeof(input_buff));
    if (input_result != true) {
        if (input_result == INPUT_ERR) {
            puts("Input string has illegal symbols.\n");
        } else {
            printf("Max length of input string is %i.\n\n", INPUT_LINE_LEN);
        }
        goto continue_point;
    }

    char *rest = input_buff;
    cmd = strtok_r(rest, " ", &rest);
    input_right = rest;

//    char *name = "fl";
//    char *result = malloc(sizeof(char) * 15);
//    for (int i = 0; i < 100; i++) {
//        sprintf(result, "%s%d.txt", name, i);
//        md(result);
//    }
//    exit(50);

    if (!cmd) {
        goto continue_point;
    }

    if (!strcasecmp(cmd, "cd")) {
        cd(input_right, current_dir);
    } else if (!strcasecmp(cmd, "dir")) {
        dir();
    } else if (!strcasecmp(cmd, "cls")) {
        system("clear");
    } else if (!strcasecmp(cmd, "mkdir") || !strcmp(cmd, "md")) {
        md(input_right);
    } else if (!strcasecmp(cmd, "del")) {
        del(input_right);
    } else if (!strcasecmp(cmd, "ren")) {
        ren(input_right);
    } else if (!strcasecmp(cmd, "fmt")) {
        fmt(input_right);
    } else if (!strcasecmp(cmd, "help")) {
        help(input_right);
    } else if (!strcasecmp(cmd, "info")) {
        info(input_right);
    } else if (!strcasecmp(cmd, "ver")) {
        printf("%s\n\n", VERSION_INFO);
    } else if (!strcasecmp(cmd, "exit")) {
        printf("Program finished with exit code %d.\n", __exit());
        return;
    } else {
        printf("Unknown command \"%s\".\n\n", input_buff);
    }
    goto continue_point;
}

int main() {
    root_tree = calloc(1, sizeof(tree_t));
    init_tree(root_tree, 0, NULL);
    init_stack(&dir_stack);
    current_tree = root_tree;

    regcomp(&file_regex, file_pattern, REG_EXTENDED | REG_NOSUB);
    regcomp(&dir_regex,  dir_pattern,  REG_EXTENDED | REG_NOSUB);
    regcomp(&path_regex, path_pattern, REG_EXTENDED | REG_NOSUB);

    if (!load_ctlg()) {
        return CTLG_READ_ERR;
    }
    menu(dir_stack);

    regfree(&file_regex);
    regfree(&dir_regex);
    regfree(&path_regex);
    return 0;
}